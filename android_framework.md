##### [Services](http://developer.android.com/guide/topics/fundamentals/services.html)

A service can essentially take two forms:

* Started -- when an application component such as an activity starts it by calling startService(). The service should stop itself, when the operation is done.
* Bound -- when an application component binds to it by calling bindService(). Multiple components can bind to the serivce at once, but when all of them unbind, the service is destroyed.
* But, it's simply a matter of whether you implement a couple callback methods: onStartCommand() to allow components to start it and onBind() to allow binding.

> 
 **Caution**: A services runs in the same process as the application in which it is declared and in the main thread of that application, by default. So, if your service performs intensive or blocking operations while the user interacts with an activity from the same application, the service will slow down activity performance. To avoid impacting application performance, you should start a new thread inside the service.

![service_lifecycle.png](http://developer.android.com/images/service_lifecycle.png "Service Lifecycle")

* a service should either be used locally by an intent that explicitly names the service class, or an intent filter.
* a start continuation flag that onStartCommand method returns specifies how to continue a service if it is killed.
* a foreground service must provide a notification for the status bar, which is placed under the "Ongoing" heading, which means that the notification cannot be dismissed unless the service is either stopped or removed from the foreground.